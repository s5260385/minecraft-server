package fr.etwin.utils;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.sql.Date;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;

public class KubeChunk {
    public final int mX;
    public final int mY;
    public byte[] data;
    public Date created_at;

    public KubeChunk(int mX, int mY, byte[] data, boolean compressed) {
        this(mX, mY, data, compressed, new java.sql.Date(new java.util.Date().getTime()));
    }

    public KubeChunk(int mX, int mY, byte[] data, boolean compressed, Date created_at) {
        if (compressed) {
            try {
                this.data = uncompress(data);
            } catch (Exception e) {
                e.printStackTrace();
                this.data = new byte[KubeConstants.CHUNK_SIZE];
            }
        } else {
            this.data = data;
        }
        this.mX = mX;
        this.mY = mY;
        this.created_at = created_at;
    }

    // WARNING: Coords in Minecraft :
    // X/Z for WIDTH
    // Y for HEIGHT
    public KubeBlock getBlock(int x, int y, int z) {
        int index = x | (z << 8) | (y << 16);
        if (index >= data.length) {
            System.err.printf("Block is out of range in chunk (%s) %s:%s:%s - %s/%s%n", index, x, y, z, mX, mY);
            return null;
        }
        return KubeBlock.from(data[index]);
    }

    public static byte[] uncompress(byte[] bytes) throws Exception {
        byte flag = bytes[0];
        InputStream input = new ByteArrayInputStream(bytes, 1, bytes.length - 1);

        byte[] chunk;
        switch (flag) {
            case 0x78: // ZLIB
                input = new InflaterInputStream(input, new Inflater(false));
                chunk = input.readNBytes(KubeConstants.CHUNK_SIZE);
                break;
            case 0x11: // DEFLATE -> RLE (no break)
                input = new InflaterInputStream(input, new Inflater(true));
            case 0x10: // RLE
                chunk = uncompressRle(input);
                break;
            default:
                throw new Exception("Unknown decompress flag");
        }

        assert chunk.length == KubeConstants.CHUNK_SIZE;
        assert input.available() == 0;
        input.close();

        return chunk;
    }

    // TODO: For more fun: make a "RleInputStream" class
    public static byte[] uncompressRle(InputStream stream) throws Exception {
        ByteBuffer result = ByteBuffer.allocate(KubeConstants.CHUNK_SIZE);

        while (stream.available() > 0) {
            // read return a byte or -1 if end of stream
            int b = stream.read();
            boolean R = (b & 0b10000000) == 0b10000000;
            int size;
            if ((b & 0b01000000) == 0b00000000) {
                // R0 abcdef
                // len = abcdef
                size = b & 0b00111111;
            } else if ((b & 0b01100000) == 0b01000000) {
                // R10 abcde llllllll
                // len = abcde llllllll
                int b2 = stream.read();
                size = (b & 0b00011111) << 8 | b2;
            } else if ((b & 0b01100000) == 0b01100000) {
                // R11 abcde llllllll llllllll
                // len = abcde llllllll llllllll
                int b2 = stream.read();
                int b3 = stream.read();
                size = (b & 0b00011111) << 16 | b2 << 8 | b3;
            } else {
                throw new Exception("Invalid RLE header");
            }

            if (size == 0) size = 1 << 20;
            assert size > 0;

            if (R) {
                byte block = (byte) stream.read();
                for (long j = 0; j < size; j++) {
                    result.put(block);
                }
            } else {
                byte[] sequence = stream.readNBytes(size);
                result.put(sequence);
            }
        }

        assert result.position() == KubeConstants.CHUNK_SIZE;
        return result.array();
    }
}
